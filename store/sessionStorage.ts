import Vuex from 'vuex';

export const state = () => ({
    search: false,
    searchValue: null,
    authenticated: false,
    user: null,
    roles: null,
    allUsers: null
});

export const mutations = {
    setSearch (state: any, search: boolean) {
        state.search = search;
    },
    setSearchValue (state: any, searchValue: String) {
        state.searchValue = searchValue;
    },
    setAuthenticated (state:any, value:boolean) {
        state.authenticated = value;
    },
    setUser (state:any, user:any) {
        state.user = user;
    },
    setRoles (state: any, roles: any[]) {
        state.roles = roles;
    },
    setAllUsers (state: any, allUsers: any[]) {
        state.allUsers = allUsers;
    }
};
