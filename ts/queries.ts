const servicesData = `
    services {
        id
        title
        description
        images {
          src
          alt
      }
  }

`;

const titulos = `
  titulos {
    cod_titulo
    nombre_titulo
    capitulo {
      cod_capitulo
      nombre_capitulo
      articulo {
        nombre_articulo
        cod_articulo
        descripcion_articulo
        nota
        parrafo {
          cod_parrafo
          contenido
        }
        paragrafo {
          cod_paragrafo
          contenido
        }
      }
    }
  }
`;

const capitulos = `
    capitulos {
        cod_capitulo
        nombre_capitulo
    }
`;


const articulos = `
articulos {
  nombre_articulo
  cod_articulo
  descripcion_articulo
  nota
  parrafo {
    cod_parrafo
    contenido
  }
  paragrafo {
    cod_paragrafo
    contenido
  }
}
`;

// const articulos = `
//     articulos {
//       nombre_articulo
//       nombre_titulo
//       capitulo {
//         cod_capitulo
//         nombre_capitulo
//       }
//     }
// `;
const images = `query {
    images {
        src
        alt
    }
}`;

const queryAuthUser = `query ($user: String!, $password: String!) {
    users (where: { user: $user, password: $password }) {
        firstName
        lastName
        user
        role {
            name
            menu_option {
                name
                path
            }
        }
        email
        birthDate
        address
        city
        password
        phone
    }
}`;

const queryAllUsers = `
    users {
        id
        user
        firstName
        lastName
        birthDate
        address
        email
        city
        phone
        role {
            name
        }
        descripcion
        services {
            id
            title
            description
            images {
                src
                alt
            }
        }
        photo
    }`;

const queryRoles = `
    options {
        id
        name
    }`;

const benefitData = `
    benefits {
        name
        image
        icon
        shortDescription
        longDescription
    }
`;
function getQuery( queries: String[] ) {
    return `query {
        ${ queries.join('\n') }
    }`;
}

export {
    servicesData,
    images,
    queryAuthUser,
    queryAllUsers,
    queryRoles,
    getQuery,
    benefitData,
    titulos,
    capitulos,
    articulos
};
