function apiFetch (query: string, variables: any) {
    return fetch(`https://${process.env.APPLICATION_API}/${process.env.PROJECT_ENVIRONMENT}/api`, {
        body: JSON.stringify({
            query: query,
            variables: variables
        }),
        method: 'post',
        headers: {
            Authorization: `client_id ${process.env.CLIENT_ID}`
        }
    }).then( resp => resp.json() );
}

function uploadFetch(data: any) {
    return fetch(`https://${process.env.APPLICATION_API}/${process.env.PROJECT_ENVIRONMENT}/upload`, {
        method: 'post',
        body: JSON.stringify({ image: data }),
        headers: {
            Authorization: `client_id ${process.env.CLIENT_ID}`
        }
    }).then( resp => resp.json() );
}

export {
    apiFetch,
    uploadFetch
}