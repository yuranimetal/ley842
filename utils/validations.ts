import { getObjs } from './defaults';

function isEmpty (nameField: string, val: any) {
    if (!val || ((val instanceof String) && val.trim()==='') || (val instanceof Array && val.length===0)) {
        return `Ingrese o seleccione el valor del campo ${ nameField }.`;
    }
    return null;
}

function isEmail (nameField: string, val: string) {
    if (!val || !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(val)) {
        return `${ nameField } no cumple con el formato de un email.`;
    }
    return null;
}

function isNumeric (nameField: string, val: string) {
    if (val && !/[0-9]/.test(val.trim())) {
        return `${ nameField } debe ser numerico.`;
    }
    return null;
}

function isRangeLength (nameField: string, val: string, min: number, max: number) {
    if (val && min && !max && val.length<min) {
        return `${ nameField } debe tener al menos ${ min } caracteres.`;
    } else if (val && !min && max && val.length>max) {
        return `${ nameField } debe tener maximo ${ max } caracteres.`;
    } else if (val && min && max && (val.length<min || val.length>max)) {
        return `${ nameField } debe tener entre ${ min } y ${ max } caracteres`;
    }
    return null;
}

function existVal (nameField: string, val: string, listObj: any[], strict: Boolean = true) {
    if (getObjs(val, listObj, strict, false)) {
        return `Ya se encontro un valor similar al del campo ${ nameField }.`;
    }
    return null;
}

function isConfPassword (nameField: string, val: string, val2: string) {
    if (val && val2 && val!==val2) {
        return `Los campos ${ nameField } no son iguales.`;
    }
    return null;
}

export {
    isEmpty,
    isNumeric,
    isEmail,
    isRangeLength,
    isConfPassword,
    existVal
}