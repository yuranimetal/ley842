function showAlert (msg: string, type: string, parent: any, next: any) {
    parent.$swal({
        icon: type,
        title: msg,
        timer: 3000,
        toast: true,
        timerProgressBar: true,
        showConfirmButton: false,
        position: 'center',
        onAfterClose: () => {
            if (next && next!=undefined) {
                next();
            }
        }
    });
}
export {
    showAlert
} 