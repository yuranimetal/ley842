import Vue from 'vue';
import Compressor from 'compressorjs';

const CompressionPlugin = {
    // @ts-ignore
    install (Vue) {
        Vue.prototype.$Compressor = Compressor;
    }
};

export default () => {
    Vue.use(CompressionPlugin);
};
