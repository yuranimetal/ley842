# dar-servicios

> Project that allows people to register, provide and acquir e informal jobs (services)

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

Libraries for styles:
* https://vuematerial.io/components/app
* https://bootstrap-vue.js.org/docs/components/

For animatios:

* https://daneden.github.io/animate.css/